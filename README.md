# Apache2 Dockerfile

## Short description
The Apache HTTP Server Project is an effort to develop and maintain an open-source HTTP server for modern operating systems including UNIX and Windows.  
The goal of this project is to provide a secure, efficient and extensible server that provides HTTP services in sync with the current HTTP standards.  

## How to choose an image
This repository contains actually 2 branches :  

Branch name | Distribution release | Apache release
------------|----------------------|------------------
apache-2.2  | Debian Wheezy (7.11) | 2.2.22-13+deb7u12
apache-2.4  | Debian Stretch (9.2) | 2.4.25-3+deb9u3

## How to build an image
* Clone the repository
```shell
git clone ${this_crazy_repository}
```
* Checkout the selected branch
```shell
git checkout apache-${your_favorite_apache_version}
```
* Get the code and build your own image
```shell
docker build --rm -t ${docker_image_name}:${docker_tag_name} .
```

Have fun and enjoy ;)
